#include <iostream>
using namespace std;

class Stack
{
private:
	static const int MAX = 10;
	int* p = new int [MAX];
	int top; // ������� ������� �����
public:
Stack() 
	{
		top = -1;
	}
	void push(int var) // �������� � ���� ����� �������
	{
		p[++top] = var;
	}
	int pop() // ������� �� ����� ������� �������
	{
		return p[top--];
	}
};

int main()
{
	Stack st1;
	st1.push(123);
	st1.push(777);
	st1.push(975);
	st1.push(543);
	st1.push(666);
	st1.push(189);
	cout << "1 element: " << st1.pop() << endl;
	cout << "2 element: " << st1.pop() << endl;
	cout << "3 element: " << st1.pop() << endl;
	cout << "4 element: " << st1.pop() << endl;
	cout << "5 element: " << st1.pop() << endl;
	cout << "6 element: " << st1.pop() << endl;
	return 0;
}